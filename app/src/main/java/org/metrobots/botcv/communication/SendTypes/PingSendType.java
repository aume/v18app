package org.metrobots.botcv.communication.SendTypes;

/**
 * Created by Sarah on 11/23/2017.
 */

public class PingSendType extends SendType {
    public String ping;

    public PingSendType() {
        //no args constructor
    }

    public PingSendType(String ping) {
        this.ping = ping;
        this.type = this.getClass().getSimpleName();
    }
}
