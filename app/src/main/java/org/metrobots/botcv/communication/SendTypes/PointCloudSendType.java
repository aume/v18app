package org.metrobots.botcv.communication.SendTypes;

/**
 * Created by Sarah on 11/23/2017.
 */

public class PointCloudSendType extends SendType {
    public int pointCount;
    public PointCloudSendType() {
    }

    public PointCloudSendType(int pointCount) {
        this.pointCount = pointCount;
        this.type = this.getClass().getSimpleName();
    }
}
