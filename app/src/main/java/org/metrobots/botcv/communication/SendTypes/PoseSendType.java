package org.metrobots.botcv.communication.SendTypes;

/**
 * Created by ctech on 11/21/17.
 */

/**
 * Please excuse disgusting naming, just Pose is awfully common and confusing af
 */
public class PoseSendType extends SendType {
    public double xTranslation;
    public double yTranslation;
    public double zTranslation;

    public double axis1Rotation;
    public double axis2Rotation;
    public double axis3Rotation;
    public double axis4Rotation;

    public double distance;
    public double angle;

    public PoseSendType() {
        //for gson
    }

    public PoseSendType(double xTranslation, double yTranslation, double zTranslation, double axis1Rotation,
                        double axis2Rotation, double axis3Rotation, double axis4Rotation) {
        this.xTranslation = xTranslation;
        this.yTranslation = yTranslation;
        this.zTranslation = zTranslation;
        this.axis1Rotation = axis1Rotation;
        this.axis2Rotation = axis2Rotation;
        this.axis3Rotation = axis3Rotation;
        this.axis4Rotation = axis4Rotation;

        /*
         * Its *probably* faster to do math on the phone and send it over to the RIO.
         * This needs to be tested
         */
        this.distance = Math.sqrt(Math.pow(xTranslation, 2.0) + Math.pow((yTranslation), 2.0));
        this.angle = Math.cos(axis1Rotation/distance);

        this.type = this.getClass().getSimpleName();
    }
}
