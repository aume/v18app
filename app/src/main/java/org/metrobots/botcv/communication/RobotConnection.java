package org.metrobots.botcv.communication;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class RobotConnection {
    public static final int K_ROBOT_PORT = 8254;
    public static final String K_ROBOT_PROXY_HOST = "127.00.00.1";
    public static final int K_CONNECTOR_SLEEP_MS = 100;
    public static final int K_THRESHOLD_HEARTBEAT = 800;
    public static final int K_SEND_HEARTBEAT_PERIOD = 100;

    private int m_port;
    private String m_host;
    private Context m_context;
    private boolean m_running = true;
    private boolean m_connected = false;
    volatile private Socket m_socket;
    private Thread m_connect_thread, m_read_thread, m_write_thread;

    private long m_last_heartbeat_sent_at = System.currentTimeMillis();
    private long m_last_heartbeat_rcvd_at = 0;

    private ArrayBlockingQueue<String> mToSend = new ArrayBlockingQueue<String>(30);

    protected class WriteThread implements Runnable {

        @Override
        public void run() {
            while (m_running) {
                String nextToSend = null;
                try {
                    nextToSend = mToSend.poll(250, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    Log.e("WriteThead", "Couldn't poll queue");
                }
                if (nextToSend == null) {
                    continue;
                }
                sendToWire(nextToSend);
            }
        }
    }

    protected class ReadThread implements Runnable {

        public void handleMessage(String message) {
            Log.d("msg", message);
        }

        @Override
        public void run() {
            while (m_running) {
                if (m_socket != null || m_connected) {
                    BufferedReader reader;
                    try {
                        InputStream is = m_socket.getInputStream();
                        reader = new BufferedReader(new InputStreamReader(is));
                    } catch (IOException e) {
                        Log.e("ReadThread", "Could not get input stream");
                        continue;
                    } catch (NullPointerException npe) {
                        Log.e("ReadThread", "socket was null");
                        continue;
                    }
                    String message = null;
                    try {
                        message = reader.readLine();
                    } catch (IOException e) {
                    }
                    if (message != null) {
                        handleMessage(message);
                    }
                } else {
                    try {
                        Thread.sleep(100, 0);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    protected class ConnectionMonitor implements Runnable {

        @Override
        public void run() {
            while (m_running) {
                try {
                    if (m_socket == null || !m_socket.isConnected() && !m_connected) {
                        tryConnect();
                        Thread.sleep(250, 0);
                    }

                    long now = System.currentTimeMillis();

                    if (now - m_last_heartbeat_sent_at > K_SEND_HEARTBEAT_PERIOD) {
                        send("heartbeat");
                        m_last_heartbeat_sent_at = now;
                    }

                    if (Math.abs(m_last_heartbeat_rcvd_at - m_last_heartbeat_sent_at) > K_THRESHOLD_HEARTBEAT && m_connected) {
                        m_connected = false;
                        broadcastRobotDisconnected();
                        broadcastWantVisionMode();
                    }
                    if (Math.abs(m_last_heartbeat_rcvd_at - m_last_heartbeat_sent_at) < K_THRESHOLD_HEARTBEAT && !m_connected) {
                        m_connected = true;
                        broadcastRobotConnected();
                    }

                    Thread.sleep(K_CONNECTOR_SLEEP_MS, 0);
                } catch (InterruptedException e) {
                }
            }

        }
    }

    public RobotConnection(Context context, String host, int port) {
        m_context = context;
        m_host = host;
        m_port = port;
    }

    public RobotConnection(Context context) {
        this(context, K_ROBOT_PROXY_HOST, K_ROBOT_PORT);
    }

    synchronized private void tryConnect() {
        if (m_socket == null) {
            try {
                Log.w("RobotConnector", "Hello World!");
                m_socket = new Socket(m_host, m_port);
                Log.w("RobotConnector", "socket: " + m_socket.getLocalSocketAddress());
                Log.w("RobotConnector", "port: " + m_socket.getLocalPort());
                m_socket.setSoTimeout(100);
            } catch (IOException e) {
                Log.w("RobotConnector", e.getMessage());
                Log.w("RobotConnector", "Could not connect");
                m_socket = null;
            }
        }
    }

    synchronized public void stop() {
        m_running = false;
        if (m_connect_thread != null && m_connect_thread.isAlive()) {
            try {
                m_connect_thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (m_write_thread != null && m_write_thread.isAlive()) {
            try {
                m_write_thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (m_read_thread != null && m_read_thread.isAlive()) {
            try {
                m_read_thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    synchronized public void start() {
        m_running = true;

        if (m_write_thread == null || !m_write_thread.isAlive()) {
            m_write_thread = new Thread(new WriteThread());
            m_write_thread.start();
        }

        if (m_read_thread == null || !m_read_thread.isAlive()) {
            m_read_thread = new Thread(new ReadThread());
            m_read_thread.start();
        }

        if (m_connect_thread == null || !m_connect_thread.isAlive()) {
            m_connect_thread = new Thread(new ConnectionMonitor());
            m_connect_thread.start();
        }
    }


    synchronized public void restart() {
        stop();
        start();
    }

    synchronized public boolean isConnected() {
        return m_socket != null && m_socket.isConnected() && m_connected;
    }

    private synchronized boolean sendToWire(String message) {
        if (m_socket != null && m_socket.isConnected()) {
            try {
                OutputStream os = m_socket.getOutputStream();
                os.write((message + "\n").getBytes());
                return true;
            } catch (IOException e) {
                Log.w("RobotConnection", "Could not send data to socket, try to reconnect");
                m_socket = null;
            }
        }
        return false;
    }

    public synchronized boolean send(String message) {
        return mToSend.offer(message);
    }

    public void broadcastRobotConnected() {

    }

    public void broadcastShotTaken() {
    }

    public void broadcastWantVisionMode() {
        }

    public void broadcastWantIntakeMode() {
       }

    public void broadcastRobotDisconnected() {
        }
}
